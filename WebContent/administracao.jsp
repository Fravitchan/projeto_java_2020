<jsp:include page="header.jsp" />
<body>
	<%@page import="models.Usuario"%>
	<%@page import="models.Quarto"%>
	<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<body>
	<%

	Usuario usuarioRead = new Usuario();
	String todosUsuarios = usuarioRead.listAll();
	String todosOsHospides = usuarioRead.listarHospedes();
	
	Quarto quartoRead = new Quarto();
	String todosQuartos = quartoRead.listAll();
	String quartoLivre = quartoRead.listQuartoProcesso("1");
	String quartoReservado = quartoRead.listQuartoProcesso("2");
	String quartoCheckIn = quartoRead.listQuartoProcesso("3");
	String quartoPago = quartoRead.listQuartoProcesso("4");
	String quartoCheckOut = quartoRead.listQuartoProcesso("5");
	
	%>


<div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/16.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h2 class="page-title">ADMINISTRAÇÃO</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Administração</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<header class="header-area">
		<!-- Search Form -->
		<div class="search-form d-flex align-items-center"></div>


		<!-- Main Header Start -->
		<div class="main-header-area">
			<div class="classy-nav-container breakpoint-off">
				<div class="container">
					<!-- Classy Menu -->
					<nav class="classy-navbar justify-content-between" id="robertoNav">

						<!-- Logo -->

						<!-- Navbar Toggler -->
						<div class="classy-navbar-toggler">
							<span class="navbarToggler"><span></span><span></span><span></span></span>
						</div>

						<!-- Menu -->
						<div class="classy-menu">
							<!-- Menu Close Button -->
							<div class="classycloseIcon">
								<div class="cross-wrap">
									<span class="top"></span><span class="bottom"></span>
								</div>
							</div>
							<!-- Nav Start -->
							<div class="classynav special-menu ">
								<ul id="nav">
									<li><a href="#">Tabelas</a>
										<ul class="dropdown">
											<li><a data-filter=".usuarios">-Usuários</a></li>
											<li><a data-filter=".hospedes">- Hóspedes</a></li>
											<li><a data-filter=".quartos">- Quartos</a></li>
											<li><a data-filter=".quartos-in">- Check-In</a></li>
											<li><a data-filter=".quartos-livre">- Livre</a></li>
											<li><a data-filter=".quartos-out">- Check-Out</a></li>
											<li><a data-filter=".quartos-reservados">- Reservados</a></li>
											<li><a data-filter=".quartos-pago">- Pago</a></li>
										</ul></li>
								</ul>
							</div>
							<!-- Nav End -->
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<div class="row special-list table-main" style="margin-left: 150px">
		<div class="special-grid usuarios" id="grid-tabels">

			<div class="book-now-btn text-center usuarios" style="width: 150px;  margin-top: 20px;">
				<a href="sistema/cadastrarUserTipo1.jsp	" >
					<button style="width: 150px;">Inserir Usuário</button>
				</a>
			</div>
			<%=todosUsuarios%>
		</div>
		<div class="special-grid quartos" id="grid-tabels" >

			<div class="book-now-btn text-center quartos" style="width: 150px; margin-top: 20px;">
				<a href="sistema/cadastrarQuarto.jsp?tipo=2" > <button style="width: 150px;">Inserir Quarto</button>
				</a>
			</div>
			<%=todosQuartos%>
		</div>
		<div class="special-grid hospedes" id="grid-tabels">

			<div class="book-now-btn text-center hospedes" style="width: 150px;  margin-top: 20px;">
				<a href="sistema/cadastrar.jsp?tipo=1" > <button style="width: 150px;">Inserir hóspede</button>
				</a>
			</div>
			<%=todosOsHospides%>
		</div>
		<div class="special-grid quartos-livre" id="grid-tabels" >

			<%=quartoLivre%>
		</div>
		<div class="special-grid quartos-reservados" id="grid-tabels">
			<%=quartoReservado%>
		</div>
		<div class="special-grid quartos-in" id="grid-tabels" >

			<%=quartoCheckIn%>
		</div>
		<div class="special-grid quartos-pago" id="grid-tabels">
			<%=quartoPago%>
		</div>
		<div class="special-grid quartos-out" id="grid-tabels">
			<%=quartoCheckOut%>
		</div>
	</div>

</body>

<!-- Start Footer  -->
<jsp:include page="footer.jsp" />
<script src="js/modules/bs-custom-file-input.js"></script>
<!-- End Footer  -->
</html>
