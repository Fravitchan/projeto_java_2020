<% 

	Cookie[] c = request.getCookies();
	for (Cookie cc : c) {
		Cookie deleteCookie = new Cookie(cc.getName(), null);
		deleteCookie.setMaxAge(0);
		response.addCookie(deleteCookie);
	}
	response.sendRedirect("index.jsp");


 %>