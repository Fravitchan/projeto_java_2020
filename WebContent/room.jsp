<%@page import="java.sql.ResultSet"%>
<%@page import="models.Quarto"%>
<jsp:include page="header.jsp" /> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%


Quarto quartoRead = new Quarto();
ResultSet rs = quartoRead.selectByProcess("1"); 

rs.next();
String nmQuarto = rs.getString("nm_quarto");
String andar = rs.getString("andar");
String nmCama = rs.getString("nm_cama");
String preco = rs.getString("preco");
String numeroQuarto = rs.getString("numero_quarto");
String foto = rs.getString("foto");
String tpCama = rs.getString("tp_cama");
String nmQuarto2 = rs.getString("nm_quarto");


%>
    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/16.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content text-center">
                        <h2 class="page-title">Nossos Quartos</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Quartos</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->

    <!-- Rooms Area Start -->
    <div class="roberto-rooms-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <!-- Single Room Area -->
                    <div class="single-room-area d-flex align-items-center mb-50 wow fadeInUp" data-wow-delay="100ms">
                        <!-- Room Thumbnail -->
                        <div class="room-thumbnail">
                            <img src="img/bg-img/<%=foto %>" alt="">
                        </div>
                        <!-- Room Content -->
                        <div class="room-content">
                            <h2><%=nmQuarto %></h2>
                            <h4>R$<%=preco %> <span>/ Dia</span></h4>
                            <div class="room-feature">
                                <h6>Andar: <span><%=andar %></span></h6>
                                <h6>Quantidade de Camas: <span><%=nmCama %></span></h6>
                                <h6>Cama: <span><%=tpCama %></span></h6>
                                <h6>Numero do Quarto: <span><%=numeroQuarto %></span></h6>
                            </div>
                            <a href="./single-room.jsp" class="btn view-detail-btn">Ver Detalhes <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <!-- Single Room Area -->
                    <div class="single-room-area d-flex align-items-center mb-50 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Room Thumbnail -->
                        <div class="room-thumbnail">
                            <img src="img/bg-img/44.jpg" alt="">
                        </div>
                        <!-- Room Content -->
                        <div class="room-content">
                            <h2><%=nmQuarto2 %></h2>
                            <h4>R$ 400 <span>/ Dia</span></h4>
                            <div class="room-feature">
                                <h6>Tamanho: <span>30 ft</span></h6>
                                <h6>Capacidade: <span>Max persion 5</span></h6>
                                <h6>Camas: <span>King beds</span></h6>
                                <h6>Serviços: <span>Wifi, television ...</span></h6>
                            </div>
                            <a href="#" class="btn view-detail-btn">Ver Detalhes <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <!-- Single Room Area -->
                    <div class="single-room-area d-flex align-items-center mb-50 wow fadeInUp" data-wow-delay="500ms">
                        <!-- Room Thumbnail -->
                        <div class="room-thumbnail">
                            <img src="img/bg-img/45.jpg" alt="">
                        </div>
                        <!-- Room Content -->
                        <div class="room-content">
                            <h2>Premium King Room</h2>
                            <h4>R$ 400 <span>/ Dia</span></h4>
                            <div class="room-feature">
                                <h6>Tamanho: <span>30 ft</span></h6>
                                <h6>Capacidade: <span>Max persion 5</span></h6>
                                <h6>Camas: <span>King beds</span></h6>
                                <h6>Serviços: <span>Wifi, television ...</span></h6>
                            </div>
                            <a href="#" class="btn view-detail-btn">Ver Detalhes <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <!-- Single Room Area -->
                    <div class="single-room-area d-flex align-items-center mb-50 wow fadeInUp" data-wow-delay="700ms">
                        <!-- Room Thumbnail -->
                        <div class="room-thumbnail">
                            <img src="img/bg-img/46.jpg" alt="">
                        </div>
                        <!-- Room Content -->
                        <div class="room-content">
                            <h2>Room Vip King</h2>
                            <h4>R$ 400 <span>/ Dia</span></h4>
                            <div class="room-feature">
                                <h6>Tamanho: <span>30 ft</span></h6>
                                <h6>Capacidade: <span>Max persion 5</span></h6>
                                <h6>Camas: <span>King beds</span></h6>
                                <h6>Serviços: <span>Wifi, television ...</span></h6>
                            </div>
                            <a href="#" class="btn view-detail-btn">Ver Detalhes <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <!-- Single Room Area -->
                    <div class="single-room-area d-flex align-items-center mb-50 wow fadeInUp" data-wow-delay="900ms">
                        <!-- Room Thumbnail -->
                        <div class="room-thumbnail">
                            <img src="img/bg-img/47.jpg" alt="">
                        </div>
                        <!-- Room Content -->
                        <div class="room-content">
                            <h2>Royal Room</h2>
                            <h4>R$ 400 <span>/ Dia</span></h4>
                            <div class="room-feature">
                                <h6>Tamanho: <span>30 ft</span></h6>
                                <h6>Capacidade: <span>Max persion 5</span></h6>
                                <h6>Camas: <span>King beds</span></h6>
                                <h6>Serviços: <span>Wifi, television ...</span></h6>
                            </div>
                            <a href="#" class="btn view-detail-btn">Ver Detalhes <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>

                <div class="col-12 col-lg-4">
                    <!-- Hotel Reservation Area -->
                    <div class="hotel-reservation--area mb-100">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Rooms Area End -->

<jsp:include page="footer.jsp" />