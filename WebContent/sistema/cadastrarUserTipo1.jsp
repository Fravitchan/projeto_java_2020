<%@page contentType="text/html" pageEncoding="UTF-8"%>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="../css/login.css" type="text/css"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Title -->
<title>Beauty Amis - admin</title>

<!-- Favicon -->
<link rel="shortcut icon" href="../img/core-img/beautyamislogo2.png">

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="../img/core-img/beautyamislogo2.png" id="icon" alt="Beauty Amis"/>
    </div>

    <!-- Login Form -->
    <form action="../inserir.jsp">
      <input type="email" id="login" class="fadeIn second"  name="email"placeholder="E-mail">
      <input type="password" id="password" class="fadeIn third" name="senha" placeholder="Senha">
      <input type="text" id="password" class="fadeIn third"  name="nome"placeholder="Nome">
      <input type="text" id="password" class="fadeIn third" name="cpf"  placeholder="CPF">
      <input type="text" id="password" class="fadeIn third" name="endereco" placeholder="Endereço">
      <input type="text" id="password" class="fadeIn third" name="cep" placeholder="CEP">
      <input type="text" id="password" class="fadeIn third" name="uf" placeholder="UF">
      <input type="text" id="password" class="fadeIn third" name="cidade" placeholder="Cidade">
      <input type="text" id="password" class="fadeIn third" name="bairro" placeholder="Bairro">
      <input type="text" id="password" class="fadeIn third" name="telefone" placeholder="Telefone">
      <input type="hidden" name="idUsuario" value="0">
      <input type="hidden" name="idNivelUsuario" value="1">
      <input type="file" name="foto" class="fadeIn third" >
      <button  type="submit" class="fadeIn fourth" >Cadastrar Usuario Tipo Administrador</button>
    </form>

    <!-- Remind Passowrd -->
    
  </div>
</div>