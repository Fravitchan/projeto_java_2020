<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="models.Usuario"%>
<%@page import="models.Quarto"%>
<%@page import="models.Reserva"%>
<% 
if (request.getParameter("idQuarto") != null) {
	int idQuarto = Integer.parseInt(request.getParameter("idQuarto"));
	String numeroQuarto = request.getParameter("numeroQuarto");
	String andar = request.getParameter("andar");
	String nmCama = request.getParameter("nmCama");
	int idNivelProcesso = Integer.parseInt(request.getParameter("idProcesso"));
	String preco = request.getParameter("preco");
	String nmQuarto = request.getParameter("nmQuarto");
	String tpCama = request.getParameter("tpCama");
	String foto = request.getParameter("foto");

	Quarto quartoWrite = new Quarto(idQuarto, numeroQuarto, andar, nmCama, preco, idNivelProcesso, foto,nmQuarto,tpCama);
	quartoWrite.save();
	response.sendRedirect("../administracao.jsp");

		}%>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="../css/login.css" type="text/css"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Title -->
<title>Beauty Amis - admin</title>

<!-- Favicon -->
<link rel="shortcut icon" href="../img/core-img/beautyamislogo2.png">

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="../img/core-img/beautyamislogo2.png" id="icon" alt="Beauty Amis"/>
    </div>

    <!-- Login Form -->
    <form action="cadastrarQuarto.jsp">
      <input type="text" id="login" class="fadeIn second"  name="numeroQuarto"placeholder="Numero do Quarto">
      <input type="text" class="fadeIn third" name="andar" placeholder="Andar">
      <input type="text" class="fadeIn third"  name="nmCama"placeholder="Numero de Camas">
      <input type="text" class="fadeIn third" name="preco"  placeholder="Preço">
      <input type="text" class="fadeIn third" name="idProcesso" placeholder="ID do Processo">
      <input type="text" class="fadeIn third" name="nmQuarto" placeholder="Nome do Quarto">
      <input type="text" class="fadeIn third" name="tpCama" placeholder="Tipo de Camas">
      <input type="hidden" name="idQuarto" value="0">
      <input type="file" name="foto" class="fadeIn third" >
      <input  type="submit" class="fadeIn fourth" > 
    </form>

    <!-- Remind Passowrd -->
    
  </div>
</div>