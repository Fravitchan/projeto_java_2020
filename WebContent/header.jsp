<!DOCTYPE html>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ include file="config.jsp"%>
<% 
Map<String, String> cookie_list = new HashMap<String, String>();
Cookie[] cookies = request.getCookies();
for (int a = 0; a < cookies.length; a++) {
cookie_list.put(cookies[a].getName(), cookies[a].getValue());
}
String teste = cookie_list.get("idNivelUsuario");

%>
<html lang="pt">

<head>
<meta charset="UTF-8">
<meta name="description"
	content="Projeto Java Web 2020 - Fl�via e Beatriz">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="projeto java" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Title -->
<title>Beauty Amis</title>

<!-- Favicon -->
<link rel="shortcut icon" href="./img/core-img/logobeautyamis.png">

<!-- Stylesheet -->
<link rel="stylesheet" href="style.css">

</head>

<body>
	<!-- Preloader -->
	<div id="preloader">
		<div class="loader"></div>
	</div>
	<!-- /Preloader -->

	<!-- Header Area Start -->
	<header class="header-area">
		<!-- Search Form -->
		<div class="search-form d-flex align-items-center">
			<div class="container">
				<form action="index.jsp" method="get">
					<input type="search" name="search-form-input" id="searchFormInput"
						placeholder="Pesquisar...">
					<button type="submit">
						<i class="icon_search"></i>
					</button>
				</form>
			</div>
		</div>

		<!-- Top Header Area Start -->
		<div class="top-header-area">
			<div class="container">
				<div class="row">

					<div class="col-6">
						<div class="top-header-content">
							<a target="_blank"
								href=""><i
								class="fa fa-whatsapp"></i> <span><%=whatsapp %></span></a> <a
								href="#"><i class="icon_mail"></i> <span><%=email %></span></a>
						</div>
					</div>

					<div class="col-6">
						<div class="top-header-content">
							<!-- Top Social Area -->
							<div class="top-social-area ml-auto">
								<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								<%
							if (cookie_list.get("login") == null || cookie_list.get("login") == "") {%>

								<a class="btn-logar" href="sistema/index.jsp?login=2" title="Login"><i
									class="fa fa-user"></i></a>
								<%}else{ %>
								<a class="btn-logar" href="sair.jsp" title="SingOut"><i
									class="fa fa-sign-out"></i></a>
									<%
							if (teste.equals("1")) {%>
									<a href="administracao.jsp">Administra��o</a>
									</a><%}} %>
								
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- Top Header Area End -->

		<!-- Main Header Start -->
		<div class="main-header-area">
			<div class="classy-nav-container breakpoint-off">
				<div class="container">
					<!-- Classy Menu -->
					<nav class="classy-navbar justify-content-between" id="robertoNav">

						<!-- Logo -->
						<a class="nav-brand" href="index.jsp"><img
							src="./img/core-img/logobeautyamis.png" alt="Logo Beauty Amis"
							width="50%" height="50%"></a>

						<!-- Navbar Toggler -->
						<div class="classy-navbar-toggler">
							<span class="navbarToggler"><span></span><span></span><span></span></span>
						</div>

						<!-- Menu -->
						<div class="classy-menu">
							<!-- Menu Close Button -->
							<div class="classycloseIcon">
								<div class="cross-wrap">
									<span class="top"></span><span class="bottom"></span>
								</div>
							</div>
							<!-- Nav Start -->
							<div class="classynav">
								<ul id="nav">
									<li class="active"><a href="index.jsp">Home</a></li>
									
									<li><a href="./room.jsp">Quartos</a></li>
									<li><a href="#">Site</a>
										<ul class="dropdown">
											<li><a href="./index.jsp">- Home</a></li>
											<li><a href="./room.jsp">- Todos os quartos</a></li>
											<li><a href="./single-room.jsp">- Quarto</a></li>
											<li><a href="./about.jsp">- Sobre n�s</a></li>
											<li><a href="./contact.jsp">- Contato</a></li>
										</ul></li>

									<li><a href="./contact.jsp">Contato</a></li>
								</ul>

								<!-- Search -->
								<div class="search-btn ml-4">
									<i class="fa fa-search" aria-hidden="true"></i>
								</div>

								<!-- Book Now -->
								<div class="book-now-btn ml-3 ml-lg-5">
									<a href="./room.jsp">Ver quartos <i
										class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div>
							</div>
							<!-- Nav End -->
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<!-- Header Area End -->