package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.DBQuery;

public class Quarto {

	
	private int idQuarto;
	private String numeroQuarto;
	private String andar;
	private String nmCama;
	private String nmQuarto;
	private String preco;
	private String tpCama;
	private String foto;
	private int idProcesso;
	



	private String tableName  = "quarto";
	private String fieldsName = "id_quarto,numero_quarto, andar,nm_cama,preco,id_processo,nm_quarto,tp_cama,foto";
	private String fieldKey   = "id_quarto";
	 
	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	 
	public Quarto() {
		
	}
	
	
	public Quarto(int idQuarto, String numeroQuarto, String andar, String nmCama, String preco, int idProcesso, String foto, String nmQuarto, String tpCama) {
		this.setIdQuarto(idQuarto);
		this.setNumeroQuarto(numeroQuarto);
		this.setAndar(andar);
		this.setNmCama(nmCama);
		this.setPreco(preco);
		this.setIdProcesso(idProcesso);
		this.setFoto(foto);
		this.setNmQuarto(nmQuarto);
		this.setTpCama(tpCama);;
		
	}
	
	private String[] toArray(){
		String[] data = new String[] {
		Integer.toString(this.getIdQuarto()),
		this.getNumeroQuarto(),
		this.getAndar(),
		this.getNmCama(),
		this.getPreco(),
		Integer.toString(this.getIdProcesso()),
		this.getNmQuarto(),
		this.getTpCama(),
		this.getFoto(),
		};
		return data;
	}
	
	public String getTpCama() {
		return tpCama;
	}


	public void setTpCama(String tpCama) {
		this.tpCama = tpCama;
	}

	public String getFoto() {
		return foto;
	}


	public void setFoto(String foto) {
		this.foto = foto;
	}

	public int getIdQuarto() {
		return idQuarto;
	}
	public void setIdQuarto(int idQuarto) {
		this.idQuarto = idQuarto;
	}
	public String getNumeroQuarto() {
		return numeroQuarto;
	}
	public void setNumeroQuarto(String numeroQuarto) {
		this.numeroQuarto = numeroQuarto;
	}
	public String getAndar() {
		return andar;
	}
	public void setAndar(String andar) {
		this.andar = andar;
	}
	public String getNmCama() {
		return nmCama;
	}
	public void setNmCama(String nmCama) {
		this.nmCama = nmCama;
	}
	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	public int getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}
	public String getNmQuarto() {
		return nmQuarto;
	}


	public void setNmQuarto(String nmQuarto) {
		this.nmQuarto = nmQuarto;
	}
	
	public ResultSet selectOneById(String id) throws SQLException {
		ResultSet rs = this.dbQuery.select(fieldKey +"="+ id);
		
		return rs;

	}
	
	
	public ResultSet selectByProcess(String id) {
		String query =  "SELECT * FROM quarto WHERE id_processo =" + id;
		ResultSet rs = dbQuery.query(query);
		return rs;
	}
	
	public void save() {
		if ( this.getIdQuarto() > 0 ) {
			this.dbQuery.update( this.toArray() );
		} else {
			this.dbQuery.insert( this.toArray() );
		}
	}

	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";
		saida +="<tr> <td class='grid-tabels-td text-center'>Id Quarto</td><td class='grid-tabels-td text-center'>Id Processo</td><td class='grid-tabels-td text-center'>Nome Quarto</td>"
				+ "<td class='grid-tabels-td text-center'>Andar</td><td class='grid-tabels-td text-center'>Numero Cama</td><td class='grid-tabels-td text-center'>Pre�o</td>"
				+ "<td class='grid-tabels-td text-center'>Numero Quarto</td><td class='grid-tabels-td text-center'>Tipo de Camas</td><td class='grid-tabels-td text-center'>Foto</td> "
				+ "<td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";
		
		
			while (rs.next()) {
				
				saida += "<tr>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_processo") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nm_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("andar") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nm_cama") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("preco") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("numero_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("tp_cama") + "</td>";
				saida += "<td class='grid-tabels-td text-center'style=' width: 100px;'><img src='img/bg-img/"+rs.getString("foto") + " 'style=width:100px;></td>";
				saida += "<td style='width:100px; height: 70px;' ><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='sistema/editar.jsp?tipo=1&id="+ rs.getString("id_quarto")+"'>Editar</button></div></td>";
				saida += "<td style='width:100px; height: 70px;'><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='deletar.jsp?tipo=1&id="+ rs.getString("id_quarto")+"'>Excluir</button></div></td>";
				saida += "</tr>";
				System.out.println();
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	
	public String listQuartoProcesso(String where) throws SQLException {
		String query =  "SELECT * FROM quarto WHERE id_processo =" + where;
		ResultSet rs = dbQuery.query(query);
		String saida = "<br>";
		saida += "<table border=1>";
		saida +="<tr> <td class='grid-tabels-td text-center'>Id Quarto</td><td class='grid-tabels-td text-center'>Id Processo</td><td class='grid-tabels-td text-center'>Nome Quarto</td>"
				+ "<td class='grid-tabels-td text-center'>Andar</td><td class='grid-tabels-td text-center'>Numero Cama</td><td class='grid-tabels-td text-center'>Pre�o</td>"
				+ "<td class='grid-tabels-td text-center'>Numero Quarto</td><td class='grid-tabels-td text-center'>Tipo de Camas</td><td class='grid-tabels-td text-center'>Foto</td> "
				+ "<td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";
		
		
			while (rs.next()) {
				
				saida += "<tr>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_processo") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nm_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("andar") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nm_cama") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("preco") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("numero_quarto") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("tp_cama") + "</td>";
				saida += "<td class='grid-tabels-td text-center'style=' width: 200px;'><img src='img/bg-img/"+rs.getString("foto") + " 'style=width:100px;></td>";
				saida += "<td style='width:100px; height: 70px;' ><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='sistema/editar.jsp?tipo=1&id="+ rs.getString("id_quarto")+"'>Editar</button></div></td>";
				saida += "<td style='width:100px; height: 70px;'><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='deletar.jsp?tipo=1&id="+ rs.getString("id_quarto")+"'>Excluir</button></div></td>";
				saida += "</tr>";
				System.out.println();
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	public void deleteQuarto(String id) {
		dbQuery.delete(id);
		
	}
	
	public void insertAnimal(String [] values) {
		dbQuery.insert(values);
	}

	public ResultSet allData() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		return rs;
	}
	
}
	
