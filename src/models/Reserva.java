package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import util.DBQuery;

public class Reserva {

	private int idReserva;
	private int userId;
	private int idQuarto;
	private int idProcesso;
	private int idPagamento;
	private Date data;

	private String tableName  = "adocao";
	private String fieldsName = "id_reserva, user_id, id_quarto ,id_processo, data";
	private String fieldKey   = "id_reserva";

	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	private String Date;

	public Reserva() {

	}
	
	public Reserva(int idReserva, int userId, int idQuarto, Date data, int idPagamento, int idProcesso) {
		this.setIdReserva(idReserva);
		this.setUserId(userId);
		this.setIdQuarto(idQuarto);
		this.setData(data);
		this.setIdPagamento(idPagamento);
		this.setIdProcesso(idProcesso);
	}
	
	private String[] toArray(){
		
		String[] dados = new String[] {
		Integer.toString(this.getIdQuarto()),
		Integer.toString(this.getUserId()),
		Integer.toString(this.getIdQuarto()),
		Integer.toString(this.getIdPagamento()),
		Integer.toString(this.getIdProcesso()),
		this.getData().toString(),
		};
		return dados;
	}


	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getIdQuarto() {
		return idQuarto;
	}

	public void setIdQuarto(int idQuarto) {
		this.idQuarto = idQuarto;
	}

	public int getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}


	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";


		while (rs.next()) {
			saida += "<tr>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_reserva") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("user_id") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_quarto") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_processo") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_pagamento") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("data") + "</td>";
			saida += "</tr>";
			System.out.println();
		}

		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	public void deleteReserva(String where) {
		String query =  "DELETE FROM reserva WHERE " + where;
		dbQuery.execute(query);
	}


	public int getIdPagamento() {
		return idPagamento;
	}


	public void setIdPagamento(int idPagamento) {
		this.idPagamento = idPagamento;
	}
	




}


