package models;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.DBQuery;

public class Usuario {

	private int	idUsuario;
	private String	email;
	private String	senha;
	private int	idNivelUsuario;
	private String	nome;
	private String	cpf;
	private String	endereco;
	private String	bairro;
	private String	cidade;
	private String	uf;
	private String	cep;
	private String	telefone;
	private String	foto;

	private String tableName  = "user";
	private String fieldsName = "user_id, email,senha, id_level_user, user_name, cpf, endereco, bairro, cidade, uf, cep, telefone, foto";
	private String fieldKey   = "user_id";

	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);

	public Usuario() {

	}

	public Usuario(int idUsuario, String email, String senha, int idNivelUsuario, String nome, String cpf, String endereco, String bairro, String cidade, String uf, String cep, String telefone, String foto) {
		this.setIdUsuario(idUsuario);
		this.setEmail(email);
		this.setSenha(senha);
		this.setIdNivelUsuario(idNivelUsuario);
		this.setNome(nome);
		this.setCpf(cpf);
		this.setEndereco(endereco);
		this.setBairro(bairro);
		this.setCidade(cidade);
		this.setUf(uf);
		this.setCep(cep);
		this.setTelefone(telefone);
		this.setFoto(foto);
	}

	private String[] toArray() {
		String[] data = new String[] {
				Integer.toString(this.getIdUsuario()),
				this.getEmail(),
				this.getSenha(),
				Integer.toString(this.getIdNivelUsuario()),
				this.getNome(),
				this.getCpf(),
				this.getEndereco(),
				this.getBairro(),
				this.getCidade(),
				this.getUf(),
				this.getCep(),
				this.getTelefone(),
				this.getFoto()};
		return(data);

	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getIdNivelUsuario() {
		return idNivelUsuario;
	}

	public void setIdNivelUsuario(int idNivelUsuario) {
		this.idNivelUsuario = idNivelUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public void save() {
		if ( this.getIdUsuario() > 0 ) {
			this.dbQuery.update( this.toArray() );
		} else {
			this.dbQuery.insert( this.toArray() );
		}
	}

	private void delete() {
		this.dbQuery.deleteList( this.toArray() );

	}
	public ResultSet selectOneById(String id) throws SQLException {
		ResultSet rs = this.dbQuery.select(fieldKey + "=" + id);
		return   rs;
	}

	public String listAll() throws SQLException {


		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";
		saida += "<tr><td class='grid-tabels-td text-center'>Id Usuario</td><td class='grid-tabels-td text-center'>Email</td><td class='grid-tabels-td text-center'>Senha</td><td class='grid-tabels-td text-center'>Level</td><td class='grid-tabels-td text-center'>Nome</td>"
				+"<td class='grid-tabels-td text-center'>CPF</td><td class='grid-tabels-td text-center'>Endere�o</td><td class='grid-tabels-td text-center'>Bairro</td><td class='grid-tabels-td text-center'>Cidade</td>"
				+ "<td class='grid-tabels-td text-center'>UF</td><td class='grid-tabels-td text-center'>CEP</td><td class='grid-tabels-td text-center'>Telefone</td><td class='grid-tabels-td text-center'>Foto</td><td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";

		while (rs.next()) {
			saida += "<tr>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("user_id") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("email") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("senha") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_level_user") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("user_name") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cpf") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("endereco") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("bairro") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cidade") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("uf") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cep") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("telefone") + "</td>";
			saida += "<td class='grid-tabels-td text-center' style=' width: 302px;'><img src='img/bg-img/"+rs.getString("foto") + " 'style=width:100px;></td>";
			saida += "<td style='width:100px; height: 70px;' ><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='sistema/editar.jsp?tipo=2&id="+ rs.getString("user_id")+"'>Editar</button></div></td>";
			saida += "<td style='width:100px; height: 70px;'><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='deletar.jsp?tipo=2&id="+ rs.getString("user_id")+"'>Excluir</button></div></td>";
			saida += "</tr>";
			System.out.println();
		}
		rs.close();
		saida += "</table>";
		return (saida);
	}

	public String listarHospedes() throws SQLException {
		String query =  "SELECT * FROM user WHERE id_level_user = 2" ;
		ResultSet rs = dbQuery.query(query);
		String saida = "<br>";
		saida += "<table border=1>";
		saida += "<tr><td class='grid-tabels-td text-center'>Id Usuario</td><td class='grid-tabels-td text-center'>Email</td><td class='grid-tabels-td text-center'>Senha</td><td class='grid-tabels-td text-center'>Level</td><td class='grid-tabels-td text-center'>Nome</td>"
				+"<td class='grid-tabels-td text-center'>CPF</td><td class='grid-tabels-td text-center'>Endere�o</td><td class='grid-tabels-td text-center'>Bairro</td><td class='grid-tabels-td text-center'>Cidade</td>"
				+ "<td class='grid-tabels-td text-center'>UF</td><td class='grid-tabels-td text-center'>CEP</td><td class='grid-tabels-td text-center'>Telefone</td><td class='grid-tabels-td text-center'>Foto</td><td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";

		while (rs.next()) {
			saida += "<tr>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("user_id") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("email") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("senha") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_level_user") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("user_name") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cpf") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("endereco") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("bairro") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cidade") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("uf") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cep") + "</td>";
			saida += "<td class='grid-tabels-td text-center'>"+rs.getString("telefone") + "</td>";
			saida += "<td class='grid-tabels-td text-center'style=' width: 302px;'><img src='img/bg-img/"+rs.getString("foto") + " 'style=width:100px;></td>";
			saida += "<td style='width:100px; height: 70px;' ><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='sistema/editar.jsp?tipo=2&id="+ rs.getString("user_id")+"'>Editar</button></div></td>";
			saida += "<td style='width:100px; height: 70px;'><div class='book-now-btn text-center'><button class=book-now-btn ml-3 ml-lg-5 onclick=window.location.href='deletar.jsp?tipo=2&id="+ rs.getString("user_id")+"'>Excluir</button></div></td>";
			saida += "</tr>";
			System.out.println();
		}

		rs.close();
		saida += "</table>";
		return (saida);
	}

	public void deleteUsuario(String id) {
		dbQuery.delete(id);

	}

}
