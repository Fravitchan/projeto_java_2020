package util;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.Usuario;


public class Login {
	
	
	DBQuery dbQuery = new DBQuery();
	
	
	
	public Usuario getLogin(String login, String senha) throws SQLException {
		 
		Usuario resposta = null;
		String query =  "SELECT * FROM user WHERE email ='" + login +"' AND senha= '" + senha+"'";
		ResultSet rs = dbQuery.query(query);
		
		if(rs.next()) {
			resposta = new  Usuario();
			resposta.setNome(rs.getString("user_name"));
			resposta.setEmail(rs.getString("email"));
			resposta.setIdNivelUsuario(Integer.parseInt(rs.getString("id_level_user")));
			resposta.setCpf(rs.getString("cpf"));
			resposta.setEndereco(rs.getString("endereco"));
			resposta.setBairro(rs.getString("bairro"));
			resposta.setCidade(rs.getString("cidade"));
			resposta.setUf(rs.getString("uf"));
			resposta.setCep(rs.getString("cep"));
			resposta.setTelefone(rs.getString("telefone"));
			resposta.setFoto(rs.getString("foto"));
		}
				
		
		return resposta;
	}
	
}
